# Pipeline d’intégration continue nécessaire à l’intégration du projet Java PetClinic dans sa version non microservice

Vous êtes le nouvel ingénieur DevOps de l'entreprise PetClinic. L’équipe de développement se repose sur un serveur afin de compiler l’application avant la livraison. Cependant, aucun test unitaire n’est exécuté, et les clients se plaignent de la mauvaise qualité de l’application.

Votre première mission est de mettre en place un pipeline d’intégration continue automatisé afin de compiler l’application, de lancer les tests unitaires associés, et de stocker les livrables sur un serveur afin de préparer le déploiement.

Cette mini exercice est très complet, il regroupe énormément de pratiques et vous fera voir de nombreux outils et notions :
* git & gitlab
* gestion de projet
* CI-CD avec GitLab :


`La partie CI regroupe :`
  * la gestion de projet sur Gitlab utilisant le framework Scrum 
      * creation des epics (grand item)
      * creation des user story 
      * Creation des sprints
      * Burndown chart
  * l'automatisation de build
  * l'automatisation de test
  * la gestion des fixs du bug

`La partie CD regroupe :` 
  - l'Infrastructure-as-Code : Docker & Docker compose
  - le déployement et test des codes
      * utilisation du site Play-With-Docker:  Ce site va vous permettre de créer une infrastructure Docker rapidement
      * pratique du docker swarm (cluster avec 3 managers & 2 workers)
  - l'automatisation du déploiement en production

## Objectif
L’objectif de cette activité est d’écrire le pipeline d’intégration continue nécessaire à l’intégration du projet Java PetClinic, mais dans sa version non microservice.

## Données
Les données sont le repository GitHub du projet Petclinic. Ce repository contient tous les éléments nécessaires au fonctionnement de l’application. Ces éléments sont :
* le code source ;
* les tests unitaires ;
* la documentation.

## Instructions
Clonez le repository dans votre repository GitLab, 
et mettez en place un pipeline d’intégration continue en suivant chacune des steps ci-dessous:

### Step 1 : [Planifiez votre développement](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6182806-planifiez-votre-developpement) 
* Nécessite la connaissance du framework scrum

### Step 2 : [Clonez le projet sur votre poste](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6182908-integrez-votre-code-en-continu#r-6276993)

### Step 3 : [Démarrer les services localement avec docker-compose](#)

Afin de démarrer une infrastructure entière en utilisant Docker, vous devez construire des images en exécutant la ligne de commande `./mvnw clean install -P buildDocker`. à partir de la racine de votre projet. Une fois que les images sont prêtes, vous pouvez les lancer avec une seule commande
`docker-compose up`. Si tout se passe bien, vous pouvez accéder aux services suivants à un endroit donné :
* Discovery Server - http://localhost:8761
* Config Server - http://localhost:8888
* AngularJS frontend (API Gateway) - http://localhost:8080
* Customers, Vets and Visits Services - random port, check Eureka Dashboard
* Tracing Server (Zipkin) - http://localhost:9411/zipkin/ (we use openzipkin)
* Admin Server (Spring Boot Admin) - http://localhost:9090
* Grafana Dashboards - http://localhost:3000
* Prometheus - http://localhost:9091

### Step 4 : [Mettre en place un pipeline d’intégration continue sur votre projet](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6182908-integrez-votre-code-en-continu#/id/r-6260946)

### Step 5 : [Packagez votre application pour la déployer](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6182962-garantissez-la-qualite-de-votre-code#/id/r-6270518)

PS : Il ne faut pas oublier de dire à maeven ou se trouve le registry docker.
Pour ce faire, editez le ficher `pom.xml` de votre projet et remplacez l'élément `<image>` dans la partie configuration  par:
  `<imageName>${env.CI_REGISTRY_IMAGE}/${docker.image.prefix}/${project.artifactId}</imageName>`

### Step 5 : Infrastructure-as-Code
  * [Construisez les images de votre application avec Docker](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6183070-codifiez-votre-infrastructure#/id/r-6261621)
  * [Déployez votre application avec Docker Compose](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6183070-codifiez-votre-infrastructure#/id/r-6261622)



### Step 6 :  [Déployez et testez votre code sur différents environnements](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6183137-deployez-et-testez-votre-code-sur-differents-environnements)

### Step 7 : [Monitorez votre application](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6183162-monitorez-votre-application)
  * En utilisant le serveur Prometheus

------
# Sources
------
- Openclassroom - [Mettez en place l'intégration et la livraison continues avec la démarche DevOps](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-continue-avec-la-demarche-devops)

- [Spring Petclinic Microservices](https://github.com/spring-petclinic/spring-petclinic-microservices)